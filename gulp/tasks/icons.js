const gulp = require('gulp')

module.exports = function icons() {
  return gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
    .pipe(gulp.dest('build/webfonts/'));
}
